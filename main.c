#include "./linked_list.h"
#include "./higher_order_f.h"
#include "./functions.h"
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <inttypes.h>

void print_space(int64_t);

void print_new(int64_t);

int64_t f_square(int64_t);

int64_t f_cube(int64_t);

int64_t f_sum(int64_t x, int64_t a);

int64_t f_min(int64_t x, int64_t a);

int64_t f_max(int64_t x, int64_t a);

int64_t twoTimes(int64_t x);

void basic_test(struct LinkedList* list);

struct LinkedList* initial(void);

void foreach_test(struct LinkedList* list);

void map_test(struct LinkedList* list);

void map_mut_test(struct LinkedList* list);

void foldl_test(struct LinkedList* list);

struct LinkedList* iterate_test(void);

void file_test(struct LinkedList** list);

void file_bin_test(struct LinkedList** list);

enum status {
    Ok,
    ErrorInSerialization,
    ErrorInDeserialization,
    ErrorInSaving,
    ErrorInLoading
};

struct LinkedList* initial(void) {
    int64_t digit;
    struct LinkedList* list = NULL;

    puts("Write your numbers:");
    while (scanf("%" SCNd64 "", &digit) != EOF) {
        if (!list) {
            list = list_create(digit);
        }
        else {
            list_add_front(&list, digit);
        }
    }
    return list;
}

void foreach_test(struct LinkedList* list) {
    puts("foreach with spaces");
    foreach(list, print_space);
    puts("\nforeach with new line");
    foreach(list, print_new);
}

void basic_test(struct LinkedList* list) {
    puts("");
    printf("Linked list of %zu was successfully initialized!\n", list_length(list));
    printf("Sum of the list: %" PRId64 "\n", list_sum(list));
    puts("");
}

void file_test(struct LinkedList** iter) {
    if (!save(*iter, "myfile")) {
        puts("An error occured while writing to the file");
        exit(ErrorInSaving);
    };
    puts("List is successfully saved");

    list_free(*iter);
    *iter = NULL;

    if (!load(iter, "myfile")) {
        puts("An error occured while reading from the file");
        exit(ErrorInLoading);
    }

    printf("Loading is complited, list is: ");
    foreach(*iter, print_space);
    puts("");
}

void file_bin_test(struct LinkedList** iter) {
    if (!serialize(*iter, "./myfilebin.bin")) {
        puts("An error occured while writing to the bin file");
        exit(ErrorInSerialization);
    };
    puts("Serialization is complited");

    list_free(*iter);
    *iter = NULL;

    if (!deserialize(iter, "./myfilebin.bin")) {
        puts("An error occured while reading from the bin file");
        exit(ErrorInDeserialization);
    }
    printf("Deserialization is complited, list is: ");
    foreach(*iter, print_space);
    puts("");
}

struct LinkedList* iterate_test(void) {
    struct LinkedList* iter = iterate(1, 10, twoTimes);
    printf("Iteration: ");
    foreach(iter, print_space);
    puts("");
    return iter;
}

void foldl_test(struct LinkedList* list) {
    int64_t sum_result = foldl(0, list, f_sum);
    printf("foldl sum: %" PRId64 "\n", sum_result);

    int64_t min_result = foldl(INT64_MAX, list, f_min);
    printf("foldl min: %" PRId64 "\n", min_result);

    int64_t max_result = foldl(INT64_MIN, list, f_max);
    printf("foldl max: %" PRId64 "\n", max_result);
}

void map_mut_test(struct LinkedList* list) {
    map_mut(&list, imaxabs);
    printf("Absolute values: ");
    foreach(list, print_space);
    puts("");
}

void map_test(struct LinkedList* list) {
    struct LinkedList* squares = map(list, f_square);
    struct LinkedList* cubes = map(list, f_cube);

    printf("Map square: ");
    foreach(squares, print_space);
    puts("");

    printf("Map cubes: ");
    foreach(cubes, print_space);
    puts("");

    list_free(squares);
    list_free(cubes);
}

void print_space(int64_t i) {
    printf("%" PRId64 " ", i);
}

void print_new(int64_t i) {
    printf("%" PRId64 "\n", i);
}

int64_t f_square(int64_t x) {
    return x * x;
}

int64_t f_cube(int64_t x) {
    return x * x * x;
}

int64_t f_sum(int64_t x, int64_t a) {
    return x + a;
}

int64_t f_min(int64_t x, int64_t a) {
    return ((x < a) ? x : a);
}

int64_t f_max(int64_t x, int64_t a) {
    return ((x > a) ? x : a);
}

int64_t twoTimes(int64_t x) {
    return 2 * x;
}

int main() {
    struct LinkedList* list = initial();
    basic_test(list);
    foreach_test(list);
    map_test(list);
    foldl_test(list);
    map_mut_test(list);

    puts("");

    struct LinkedList* iter = iterate_test();
    file_test(&iter);
    file_bin_test(&iter);

    list_free(list);
    list_free(iter);
    return Ok;
}
