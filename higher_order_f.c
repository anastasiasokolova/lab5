#include "./linked_list.h"
#include "./higher_order_f.h"
#include <stddef.h>
#include <stdint.h>

// запускает функцию func на каждом элементе списка list
void foreach(struct LinkedList* list, void (*func)(int64_t)) {
    while (list != NULL) {
        func(list->value);
        list = list->next;
    }
}

// создает новый список, в котором каждый элемент получен из соответсвующего элемента списка origin
struct LinkedList* map(struct LinkedList* origin, int64_t(*func)(int64_t)) {
    struct LinkedList* head = NULL;
    while (origin != NULL) {
        list_add_back(&head, func(origin->value));
        origin = origin->next;
    }
    return head;
}

// изменяет каждый элемент списка origin с помощью функции func
void map_mut(struct LinkedList** origin, int64_t(*func)(int64_t)) {
    if ((*origin) == NULL)
        return;
    (*origin)->value = func((*origin)->value);
    map_mut(&(*origin)->next, func);
    return;
}

// возвращает список list с помощью функции func
int64_t foldl(int64_t accum, struct LinkedList* list, int64_t(*func)(int64_t, int64_t)) {
    while (list != NULL) {
        accum = func(list->value, accum);
        list = list->next;
    }
    return accum;
}

// генерирует список длины n с помощью значения s и функции func
struct LinkedList* iterate(int64_t s, size_t n, int64_t(*func)(int64_t)) {
    struct LinkedList* list = list_create(s);
    unsigned int i;
    for (i = 0; i < n-1; i++) {
        s = func(s);
        list_add_back(&list, s);
    }
    return list;
}
