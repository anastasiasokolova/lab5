#include "./linked_list.h"
#include "./functions.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>

#include "./higher_order_f.h"

bool save(struct LinkedList* list, const char* filename) {
    FILE* file;
    file = fopen(filename, "w");
    if (file == NULL) {
        return false;
    }
    while (list != NULL) {
        fprintf(file, "%"PRId64" ", list->value);
        list = list->next;
    }
    fclose(file);
    return true;
}

bool load(struct LinkedList** list, const char* filename) {
    FILE* file;
    int64_t num;
    if ((file = fopen(filename, "r")) == NULL) {
        return false;
    }
    while (fscanf(file, "%"PRId64" ", &num) != EOF) {
        list_add_back(list, num);
    }
    fclose(file);
    return true;
}

bool serialize(struct LinkedList* list, const char* filename) {
    FILE* bin;
    if ((bin = fopen(filename, "wb")) == NULL) {
        return false;
    }
    while (list != NULL) {
        fwrite(&(list->value), sizeof(int64_t), 1, bin);
        list = list->next;
    }
    fclose(bin);
    return true;
}

bool deserialize(struct LinkedList** list, const char* filename) {
    FILE* bin;
    int64_t num;
    if ((bin = fopen(filename, "rb")) == NULL) {
        return false;
    }
    while (fread(&num, sizeof(int64_t), 1, bin) != 0) {
        list_add_back(list, num);
    }
    fclose(bin);
    return true;
}
