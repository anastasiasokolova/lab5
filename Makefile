GCC = gcc
GCC_FLAGS = -std=c18 -pedantic -Wall -Werror
LINKED_LIST = linked_list
MAIN = main
HIGHER_ORDER = higher_order_f
FUNCTIONS = functions

all: build

linked_list_obj: $(LINKED_LIST).c
	$(GCC) $(GCC_FLAGS) -c $(LINKED_LIST).c

higher_order_obj: $(HIGHER_ORDER).c
	$(GCC) $(GCC_FLAGS) -c $(HIGHER_ORDER).c  

functions_obj: $(FUNCTIONS).c
	$(GCC) $(GCC_FLAGS) -c $(FUNCTIONS).c  

main_obj: $(MAIN).c
	$(GCC) $(GCC_FLAGS) -c $(MAIN).c


build: linked_list_obj main_obj higher_order_obj functions_obj
	$(GCC) $(GCC_FLAGS) $(MAIN).o $(LINKED_LIST).o $(HIGHER_ORDER).o $(FUNCTIONS).o -o $(MAIN)
